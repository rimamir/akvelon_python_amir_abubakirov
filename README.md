# Transaction service - Akvelon - Amir Abubakirov
## Tech stack:Django, DRF, Swagger,Docker, docker-compose, PostgreSQL. Gitlab CI.
## In server side: Nginx, gunicorn

## Staging server
### NOTE: to use swagger, you need to log in to the admin panel

### Swagger docs
```http request
abubakirov.space/api/swagger/
```

### Admin panel
```http request
abubakirov.space/admin/
```

## Deploy to server
### Only push into gitlab, configured CI/CD

## Local install
### Install poetry
```shell
pip install poetry
```

### Build image, create and start database container
```shell
docker-compose -f docker-compose.local.yml -p transaction_service_backend up -d --build
```

### Create superuser in docker psql
```shell
docker exec -it transaction_service_backend_web_1 sh
python manage.py createsuperuser
```

### Install the project dependencies
```shell
cd src && poetry install
```

### Swagger docs
```shell
localhost:8000/api/swagger/
```
### Admin panel
```shell
localhost:8000/admin/
```

## Fibonacci calculator in src/utils.py
