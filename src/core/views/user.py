from rest_framework import viewsets
from drf_yasg.utils import swagger_auto_schema
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from core.models.user import User
from core.serializers.user import UserSerializer, UserCreateSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """

    serializer_classes = {
        "create": UserCreateSerializer,
    }
    default_serializer_class = UserSerializer  # Your default serializer

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    queryset = User.objects.all()
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ["id", "email", "first_name", "last_name"]
    ordering_fields = ["id"]

    @swagger_auto_schema(
        operation_id="user_create", request_body=UserCreateSerializer, responses={200: UserSerializer,},
    )
    def create(self, request, *args, **kwargs):
        return super().create(request)
