from django.db.models import Sum
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema, no_body
from drf_yasg import openapi

from core.models.user import User
from core.models.transaction import Transaction
from core.serializers.transaction import TransactionSerializer


class TransactionViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing transaction instances.
    """

    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ["user", "date", "amount"]
    ordering_fields = ["date", "amount"]

    start_date = openapi.Parameter("start_date", openapi.IN_QUERY, type=openapi.FORMAT_DATE)
    end_date = openapi.Parameter("end_date", openapi.IN_QUERY, type=openapi.FORMAT_DATE)

    @swagger_auto_schema(request_body=no_body, manual_parameters=[start_date, end_date])
    @action(methods=["get"], detail=False)
    def grouped(self, request, *args, **kwargs):
        query_params = self.request.query_params
        transactions_qs = self.get_queryset()

        if query_params.get("start_date") is not None:
            transactions_qs = transactions_qs.filter(date__gte=query_params.get("start_date"))
        if query_params.get("end_date") is not None:
            transactions_qs = transactions_qs.filter(date__lte=query_params.get("end_date"))
        if query_params.get("user") is not None:
            transactions_qs = transactions_qs.filter(user__id=query_params.get("user"))
        if query_params.get("date") is not None:
            transactions_qs = transactions_qs.filter(date=query_params.get("date"))

        users = User.objects.all()
        all_users_grouped_transactions = []

        for user in users:
            current_user_transactions_qs = transactions_qs.filter(user__id=user.pk)
            grouped_transactions = current_user_transactions_qs.values("date").annotate(sum=Sum("amount"))

            for transaction in grouped_transactions:
                transaction.update(user_id=user.id)
                all_users_grouped_transactions.append(transaction)

        return Response(all_users_grouped_transactions, status=status.HTTP_200_OK)
