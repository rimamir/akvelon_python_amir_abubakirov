from django.urls import path
from django.conf import settings
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view
from rest_framework import routers
from rest_framework.permissions import IsAuthenticated

from core.views.user import UserViewSet
from core.views.transaction import TransactionViewSet

app_name = "core"

router = routers.SimpleRouter()

router.register("user", UserViewSet, basename="user")
router.register("transaction", TransactionViewSet, basename="transaction")


class CustomSchemaGenerator(OpenAPISchemaGenerator):
    def get_schema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema.schemes = ["https", "http"]
        if settings.ENVIRONMENT == "local":
            schema.schemes.reverse()
        return schema


schema_view = get_schema_view(
    openapi.Info(title="Transaction service API", default_version="v1"),
    public=True,
    generator_class=CustomSchemaGenerator,
    permission_classes=(IsAuthenticated,),
)

urlpatterns = [
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui",),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]

urlpatterns += router.urls
