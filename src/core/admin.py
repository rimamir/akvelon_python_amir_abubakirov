from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from core.forms.user import UserChangeForm, UserCreationForm
from core.models.user import User
from core.models.transaction import Transaction


class UserAdmin(BaseUserAdmin):
    model = User

    add_form = UserCreationForm
    form = UserChangeForm

    list_display = (
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_active",
    )
    list_filter = (
        "email",
        "is_staff",
        "is_active",
    )
    fieldsets = (
        (None, {"fields": ("email", "first_name", "last_name", "password",)},),
        ("Permissions", {"fields": ("is_staff", "is_active")}),
    )
    add_fieldsets = (
        (None, {"classes": ("wide",), "fields": ("email", "password1", "password2", "is_staff", "is_active"),},),
    )
    search_fields = ("email",)
    ordering = ("email",)


class TransactionAdmin(admin.ModelAdmin):
    model = Transaction
    list_display = ("user", "amount", "date")
    fields = ("user", "amount", "date")


admin.site.register(User, UserAdmin)
admin.site.register(Transaction, TransactionAdmin)
