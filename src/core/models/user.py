from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.managers.user import UserManager


class User(AbstractUser):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """

    username = None
    email = models.EmailField(verbose_name=_("email"), unique=True)
    first_name = models.CharField(verbose_name=_("name"), max_length=150, blank=True)
    last_name = models.CharField(verbose_name=_("surname"), max_length=150, blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return self.email
