from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models.user import User


class Transaction(models.Model):
    user = models.ForeignKey(to=User, verbose_name=_("user"), on_delete=models.CASCADE, blank=False)
    amount = models.IntegerField(verbose_name=_("amount"), blank=False)
    date = models.DateField(verbose_name=_("date"), blank=False)

    def __str__(self):
        return f"{self.amount} - {self.date}"
