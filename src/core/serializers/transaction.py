from rest_framework import serializers

from core.models.user import User
from core.models.transaction import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = Transaction
        fields = ["id", "user_id", "amount", "date"]

    def create(self, validated_data):
        user = validated_data.pop("user_id")
        return Transaction.objects.create(user=user, **validated_data)
