from rest_framework import serializers

from core.models.user import User


class BaseUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "email", "first_name", "last_name"]

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user


class UserSerializer(BaseUserSerializer):
    pass


class UserCreateSerializer(BaseUserSerializer):
    class Meta(BaseUserSerializer.Meta):
        fields = ["id", "email", "first_name", "last_name", "password"]

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response.pop("password", None)
        return response
