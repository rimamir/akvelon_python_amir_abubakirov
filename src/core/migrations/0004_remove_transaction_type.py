# Generated by Django 3.2.3 on 2021-05-16 22:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0003_alter_transaction_type"),
    ]

    operations = [
        migrations.RemoveField(model_name="transaction", name="type",),
    ]
