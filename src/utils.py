def fibonacci(n: int) -> int:
    if n < 0:
        print("Incorrect input")
    # first fibonacci number is 0
    elif n == 0:
        return 0
    # second fibonacci number is 1
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


print(f"Fibonacci(0): {fibonacci(0)}")
print(f"Fibonacci(5): {fibonacci(5)}")
print(f"Fibonacci(9): {fibonacci(7)}\n")

print(f"Fibonacci(-1): {fibonacci(-1)}")
